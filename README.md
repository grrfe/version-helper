# version-helper

A Gradle Plugin which allows the user to extract the version number from a git tag and store it in a jar file which is
also created by this plugin. The plugin can then be used at runtime to fetch said version from the jar and use it.
