package fe.version.helper

import fe.version.helper.gradle.versionFile

class Version private constructor(
    fileName: String = "/$versionFile"
) {
    private val properties = this.javaClass.getResourceAsStream(fileName)?.bufferedReader()?.use {
        it.readLines()
    }?.associate { line -> line.split("=").let { it[0] to it[1] } }

    val build = properties?.get("VERSION_BUILD")
    val branch = properties?.get("VERSION_BRANCH")
    val base = properties?.get("VERSION_BASE")
    val branchId = properties?.get("VERSION_BRANCHID")
    val branchType = properties?.get("VERSION_BRANCHTYPE")
    val commit = properties?.get("VERSION_COMMIT")
    val gradle = properties?.get("VERSION_GRADLE")
    val display = properties?.get("VERSION_DISPLAY")
    val full = properties?.get("VERSION_FULL")
    val scm = properties?.get("VERSION_SCM")
    val tag = properties?.get("VERSION_TAG")
    val lastTag = properties?.get("VERSION_LAST_TAG")
    val dirty = properties?.get("VERSION_DIRTY")?.toBoolean()
    val versionCode = properties?.get("VERSION_VERSIONCODE")?.toIntOrNull()
    val major = properties?.get("VERSION_MAJOR")?.toIntOrNull()
    val minor = properties?.get("VERSION_MINOR")?.toIntOrNull()
    val patch = properties?.get("VERSION_PATCH")?.toIntOrNull()
    val qualifier = properties?.get("VERSION_QUALIFIER")

    companion object {
        fun readVersion(fileName: String = "/$versionFile") = Version(fileName)
    }
}
