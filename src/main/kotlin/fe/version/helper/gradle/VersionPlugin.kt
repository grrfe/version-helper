package fe.version.helper.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.plugins.JavaApplication
import org.gradle.jvm.tasks.Jar
import java.io.File
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty1
import kotlin.reflect.full.functions
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

val versionFile = "version.properties"

class VersionPlugin : Plugin<Project> {
    companion object {
        val versionMembers = listOf("tag", "full")
    }

    override fun apply(target: Project) {
        val versioning = (target as org.gradle.api.plugins.ExtensionAware).extensions.getByName("versioning")
        val infoFn = versioning::class.functions.find {
            it.name == "getInfo"
        }?.apply { isAccessible = true }?.call(versioning)


        target.version = versionMembers.map { member ->
            infoFn?.getMemberAsString(member)
        }.firstOrNull() ?: "No version found!"

        target.tasks.getByName("versionFile") {
            it.setProperty("file", File(target.buildDir, "resources/main/$versionFile"))
        }

        target.tasks.getByName("classes") {
            it.dependsOn.add(target.tasks.getByName("versionFile"))
        }

        target.tasks.withType(Jar::class.java).getByName("jar") { jar ->
            val applicationObj: JavaApplication? = (target as org.gradle.api.plugins.ExtensionAware)
                .extensions.getByName("application") as? JavaApplication

            if (applicationObj != null && applicationObj::class.members.find { it.name == "getMainClass" } != null) {
                applicationObj.mainClass.orNull?.let { mainClass ->
                    jar.manifest {
                        it.attributes["Main-Class"] = mainClass
                        jar.archiveFileName.set("${target.name}-${target.version}.jar")
                    }
                }

                jar.from(target.configurations.named("runtimeClasspath").get().map {
                    if (it.isDirectory) it else target.zipTree(it)
                })
                jar.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        }
    }

    fun Any.getMemberAsString(name: String): String? {
        return (this::class.memberProperties.find { it.name == name } as? KProperty1<Any, *>)?.apply {
            isAccessible = true
        }?.get(this) as? String
    }
}
