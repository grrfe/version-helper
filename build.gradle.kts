plugins {
    kotlin("jvm") version "1.7.10"
    java
    maven
    `java-gradle-plugin`
    id("net.nemerosa.versioning") version "3.0.0"
    id("com.gradle.plugin-publish") version "1.0.0"
}

group = "com.gitlab.grrfe.version-helper"
version = "2.1.23"

repositories {
    mavenCentral()
}

dependencies {
    api(kotlin("stdlib"))
    api("org.jetbrains.kotlin:kotlin-reflect:1.7.10")
}

gradlePlugin {
    plugins {
        create("versionHelper") {
            id = "com.gitlab.grrfe.version-helper"
            displayName = "VersionHelper"
            description = "Reads version from git tag, adds handler to read version in binary"
            implementationClass = "fe.version.helper.gradle.VersionPlugin"
        }
    }
}

pluginBundle {
    website = "https://gitlab.com/grrfe"
    vcsUrl = "https://gitlab.com/grrfe/version-helper"
    tags = listOf("version", "git", "tag")
}
